---
title: "Scrapping en R"
output: 
  html_document: 
    keep_md: yes
    toc: yes
  # html_notebook: 
  #   toc: yes
  #   keep_md: true
---

# Introducción

En este documento se presenta un ejemplo de como hacer **scrapping** _(Obtener información de una página de internet y guardarla en una tabla)_ con **R**, usando el paquete **rvest**.

-----------------------------------

# 1 Instalar _Selector Gadget_ en _Google Chrome_

Para poder ver el código detrás de un objeto en una página de internet de manera interactiva, se utiliza el **Aditamento** de _Google Chrome_ llamado **Select Gadget**.

Pasos para instalación:

  - Abrir el siguiente [link](https://chrome.google.com/webstore/detail/selectorgadget/mhjhnkcfbdhnjickkkdbjoemdmbfginb/related) en **Google Chrome**
  - Dar Click en **Añadir a Chrome**

![](/Users/darias/Desktop/ScrappingR/scrapping_files/figure-html/img1.png)

  - Sabremos que está instalado porque ahora en el navegador de _Google Chrome_ encontraremos una _lupa_ del lado superior derecho

![](/Users/darias/Desktop/ScrappingR/Imagenes/img2.png)


-------------------------------------

# 2 Instalar paquete **rvest**

Se instala el paquete **rvest** que se usará para _scrapping_


```r
#install.packages('rvest')
```


-----------------------------------------

# 3 Librerías

Se cargaran todas las librerías a usar para este ejercicio


```r
library(dplyr)   # Librería para manipulación de datos
```

```
## Warning: package 'dplyr' was built under R version 3.5.2
```

```
## 
## Attaching package: 'dplyr'
```

```
## The following objects are masked from 'package:stats':
## 
##     filter, lag
```

```
## The following objects are masked from 'package:base':
## 
##     intersect, setdiff, setequal, union
```

```r
library(rvest)   # Librería para hacer scrapping
```

```
## Loading required package: xml2
```

```r
library(ggplot2) # Librería para gráfico de datos
library(stringi)
```

```
## Warning: package 'stringi' was built under R version 3.5.2
```

------------------------------------

# 4 Ejercicio Scrapping

Para hacer _scrapping_, necesitamos saber el código fuente en _HTML_ de la página que se desea _scrappear_, con lo que **Select Gadget** de _Google_ nos será útil

Para este ejercicio en particular, se hará un _scrapping_ de **Liverpool** para conocer sobre las **Laptops**:

  - Modelo
  - Precio Original
  - Precio Oferta

--------------------------------

## 4.1 Página de Liverpool de LapTops con _Selector Gadget_

Pasos para empezar el scrapping:

  - Entramos a la [página](https://www.liverpool.com.mx/tienda/N-1yzxuh9/page-1?s=laptop) de _Laptops_ de _Liverpool_ 
  - _Click_ en la lupa del _Select Gadget_
  - _Click_ en el modelo de la primer _laptop_
![](/Users/darias/Desktop/ScrappingR/scrapping_files/figure-html/img3.png)
  - Notamos que el nombre de las _laptops_ en esta página se llama _.product-name_ en _HTML_

---------------------------------

## 4.2 Obtener los nombres de todas _laptops_

Con el paquete Rvest, conociendo la página de internet y el nombre _HTML_ del objeto que queremos _scrappear_, podemos _bajar_ la información.

Se declara el nombre de la página de internet


```r
url <- 'https://www.liverpool.com.mx/tienda/N-1yzxuh9/page-1?s=laptop'
```

--------------------------------

Se lee la página _Web_ con Rvest con la función **read_html**


```r
webpage <- read_html(url)
```

Se _scrappean_ los nombres de las _laptops_ con la función **html_nodes**


```r
lap_names_html <- html_nodes(webpage,'.product-name')
```

Los nombres _scrappeados_ se pasan a texto con la función **html_text**


```r
lap_names <- html_text(lap_names_html)
head(lap_names)
```

```
## [1] "\r\n\t\t\t Laptop 2 en 1 Lenovo Yoga 530 14 Pulgadas Intel Core i7 8 GB RAM 256 GB Disco Duro\r\t\t"          
## [2] "\r\n\t\t\t Laptop HP 15-db0004 15.6 Pulgadas AMD A9 4 GB RAM 1 TB Disco Duro\r\t\t"                           
## [3] "\r\n\t\t\t Laptop Lenovo Ideapad 530s 14 Pulgadas Intel Core i7 8 GB RAM 256 GB Disco Duro\r\t\t"             
## [4] "\r\n\t\t\t Laptop Asus Vivobook X505BA 15.6 Pulgadas AMD A9 4 GB RAM 1 TB Disco Duro\r\t\t"                   
## [5] "\r\n\t\t\t Laptop Dell Inspiron 5570 15.6 Pulgadas Intel Core i3 4 GB+16 GB Optane RAM 1 TB Disco Duro\r\t\t" 
## [6] "\r\n\t\t\t Laptop Lenovo Ideapad 330S 14 Pulgadas Intel Core i3 4 GB + 16 GB Optane RAM 1 TB Disco Duro\r\t\t"
```

Notamos que los nombres tienen caracteres que no queremos, por lo cuál los vamos a limpiar con la función **gsub()**


```r
lap_names <- gsub('\r', '', lap_names)
lap_names <- gsub('\n', '', lap_names)
lap_names <- gsub('\t', '', lap_names)
lap_names <- trimws(lap_names) # Quitamos espacios antes del primer caracter o después del último caracter
head(lap_names)
```

```
## [1] "Laptop 2 en 1 Lenovo Yoga 530 14 Pulgadas Intel Core i7 8 GB RAM 256 GB Disco Duro"          
## [2] "Laptop HP 15-db0004 15.6 Pulgadas AMD A9 4 GB RAM 1 TB Disco Duro"                           
## [3] "Laptop Lenovo Ideapad 530s 14 Pulgadas Intel Core i7 8 GB RAM 256 GB Disco Duro"             
## [4] "Laptop Asus Vivobook X505BA 15.6 Pulgadas AMD A9 4 GB RAM 1 TB Disco Duro"                   
## [5] "Laptop Dell Inspiron 5570 15.6 Pulgadas Intel Core i3 4 GB+16 GB Optane RAM 1 TB Disco Duro" 
## [6] "Laptop Lenovo Ideapad 330S 14 Pulgadas Intel Core i3 4 GB + 16 GB Optane RAM 1 TB Disco Duro"
```

Otra forma de hacer esto, es con la librería **dplyr**, uniendo todas las funciones


```r
webpage %>%
  html_nodes('.product-name') %>%
  html_text(.) %>%
  gsub('\r', '',.) %>%
  gsub('\n', '',.) %>%
  gsub('\t', '',.) %>%
  trimws(.) -> lap_names

head(lap_names)
```

```
## [1] "Laptop 2 en 1 Lenovo Yoga 530 14 Pulgadas Intel Core i7 8 GB RAM 256 GB Disco Duro"          
## [2] "Laptop HP 15-db0004 15.6 Pulgadas AMD A9 4 GB RAM 1 TB Disco Duro"                           
## [3] "Laptop Lenovo Ideapad 530s 14 Pulgadas Intel Core i7 8 GB RAM 256 GB Disco Duro"             
## [4] "Laptop Asus Vivobook X505BA 15.6 Pulgadas AMD A9 4 GB RAM 1 TB Disco Duro"                   
## [5] "Laptop Dell Inspiron 5570 15.6 Pulgadas Intel Core i3 4 GB+16 GB Optane RAM 1 TB Disco Duro" 
## [6] "Laptop Lenovo Ideapad 330S 14 Pulgadas Intel Core i3 4 GB + 16 GB Optane RAM 1 TB Disco Duro"
```

--------------------------------------------

## 4.3 Obtener los precios originales y con descuento de todas las _laptops_

De igual manera que con el nombre del producto, vemos que el nombre en _HTML_ del precio original es: _.product-price_

Donde vemos que viene tanto el precio original como el precio en descuento, siendo el primer precio el original y el segundo el de descuento,

Vemos que cuando se pasa a texto, suceden dos cosas:

  1. Trae la coma de "miles", por lo que convierte el vector en _String_, hay que removerla
  2. No pone un **punto** para separar los centavos, por lo cual hay que dividir entre 100
  3. Se deben separar en precio original y precio de descuento
  4. Quitar caracteres de sobra


```r
webpage %>%
  html_nodes('.product-price') %>%
  html_text(.) %>%
  head(2)
```

```
## [1] " \r\n\t\t\t\t\t\t\n\t\t\t\t\t\t\t\n\t\t\t\t\t\t\t Precio Sugerido:\t\n\t\t\t$\n\t\t\t\n\t\t\t25,99900\t\n\t\t\t\t\t\t\t\n\t\t\t\t\t\t\t\n\t\t\t\t\t\t\t\n\t\t\t\t\t\t\t Precio Lista:\t\n\t\t\t$\n\t\t\t\n\t\t\t23,39910\t\n\t\t\t"
## [2] " \r\n\t\t\t\t\t\t\n\t\t\t\t\t\t\t\n\t\t\t\t\t\t\t Precio Sugerido:\t\n\t\t\t$\n\t\t\t\n\t\t\t11,10900\t\n\t\t\t\t\t\t\t\n\t\t\t\t\t\t\t\n\t\t\t\t\t\t\t\n\t\t\t\t\t\t\t Precio Lista:\t\n\t\t\t$\n\t\t\t\n\t\t\t9,02250\t\n\t\t\t"
```


```r
webpage %>%
  html_nodes('.product-price') %>%
  html_text(.) %>%
  gsub('\t','',.) %>%
  gsub('\n','',.) %>%
  gsub('\\$','',.) %>%
  gsub(',','',.) %>%
  gsub('Precio Sugerido:','',.) %>%
  gsub('Precio Lista:','',.) %>%
  trimws(.) -> prices

# Se crea un vector para precio original y uno para precio con descuento
original_price <- vector()
strike_price   <- vector()

# Loop que:
#  - Separa en 2 cada fila
#  - Asigan el primer elemento a original price
#  - Asigan el segundo elemento a strike price
#  - Lo convierte a número
#  - Lo divide entre 100


for(i in 1:length(prices)){
  original_price[i] <- as.numeric(unlist(stri_split_fixed(str = prices[i], pattern = ' '))[1])/100
  strike_price[i]   <- as.numeric(unlist(stri_split_fixed(str = prices[i], pattern = ' '))[2])/100
}

original_price <- as.numeric(original_price)
strike_price <- as.numeric(strike_price)
```


```r
head(original_price)
```

```
## [1] 25999 11109 24999 10499 12999 13999
```


```r
head(strike_price)
```

```
## [1] 23399.1  9022.5 22499.1  9449.1 11699.1 12599.1
```


--------------------------------------------

## 4.4 Unir todos los datos para crear una tabla

Se unirán todas las columnas para crear una tabla con toda la información


```r
laptops <- data.frame(modelo          = lap_names,
                      precio_original = original_price,
                      precio_oferta   = strike_price)

head(laptops)
```

```
##                                                                                         modelo
## 1           Laptop 2 en 1 Lenovo Yoga 530 14 Pulgadas Intel Core i7 8 GB RAM 256 GB Disco Duro
## 2                            Laptop HP 15-db0004 15.6 Pulgadas AMD A9 4 GB RAM 1 TB Disco Duro
## 3              Laptop Lenovo Ideapad 530s 14 Pulgadas Intel Core i7 8 GB RAM 256 GB Disco Duro
## 4                    Laptop Asus Vivobook X505BA 15.6 Pulgadas AMD A9 4 GB RAM 1 TB Disco Duro
## 5  Laptop Dell Inspiron 5570 15.6 Pulgadas Intel Core i3 4 GB+16 GB Optane RAM 1 TB Disco Duro
## 6 Laptop Lenovo Ideapad 330S 14 Pulgadas Intel Core i3 4 GB + 16 GB Optane RAM 1 TB Disco Duro
##   precio_original precio_oferta
## 1           25999       23399.1
## 2           11109        9022.5
## 3           24999       22499.1
## 4           10499        9449.1
## 5           12999       11699.1
## 6           13999       12599.1
```


------------------------------------------------------------

# 5 Crear la tabla con todas las pestañas

Notamos que el _link_ de dónde hicimos el _scrapping_ es sólo la primer página de _laptops_, nos gustaría tener los nombres y precios de todas.

Vemos que el _link_ indica el número de página, por lo que con un ciclo _for_ podríamos ir adicionando la información de cada página en la tabla.

![](/Users/darias/Desktop/ScrappingR/scrapping_files/figure-html/img4.png)

-------------------------------------

## 5.1 Número de páginas

Para ver el número de páginas, usamos el _Selector Gadget_ de _Google_ en donde vienen los números de las páginas con los productos, para poder extraer automáticamente el número de páginas que hay.

El nombre en _HTML_ es: _.p-num_


```r
webpage %>%
  html_nodes('.p-num') %>%
  html_text(.) %>%
  as.numeric() %>%
  max() -> num_paginas
```

----------------------------------------

## 5.2 Creamos cascarón de tabla

Hacemos el cascarón o voceto de la tabla donde iremos adicionando la información


```r
laptops <- data.frame(modelo          = character(),
                      precio_original = numeric(),
                      precio_oferta   = numeric())
```

-----------------------------------------

## 5.3 Ciclo for para todas las páginas

Hacemos un ciclo _for_ que en cada iteración adiciona a la tabla la información de cada página


```r
for(i in 1:num_paginas){
  # Leer el link
  url <- paste0('https://www.liverpool.com.mx/tienda/N-1yzxuh9/page-',i,'?s=laptop')
  webpage <- read_html(url)
  
  # Nombres de las laptops
  webpage %>%
    html_nodes('.product-name') %>%
    html_text(.) %>%
    gsub('\r', '',.) %>%
    gsub('\n', '',.) %>%
    gsub('\t', '',.) %>%
    trimws(.) -> lap_names
  
  # Precio original y en descuento
  webpage %>%
   html_nodes('.product-price') %>%
   html_text(.) %>%
   gsub('\t','',.) %>%
   gsub('\n','',.) %>%
    gsub('\\$','',.) %>%
   gsub(',','',.) %>%
   gsub('Precio Sugerido:','',.) %>%
   gsub('Precio Lista:','',.) %>%
    trimws(.) -> prices

  # Se crea un vector para precio original y uno para precio con descuento
  original_price <- vector()
  strike_price   <- vector()

  # Loop que:
  #  - Separa en 2 cada fila
  #  - Asigan el primer elemento a original price
  #  - Asigan el segundo elemento a strike price
  #  - Lo convierte a número
  #  - Lo divide entre 100

  for(j in 1:length(prices)){
    original_price[j] <- as.numeric(unlist(stri_split_fixed(str = prices[j], pattern = ' '))[1])/100
    strike_price[j]   <- as.numeric(unlist(stri_split_fixed(str = prices[j], pattern = ' '))[2])/100
  }

  original_price <- as.numeric(original_price)
  strike_price <- as.numeric(strike_price)
  
  # Se une la información en una tabla auxiliar
  aux = data.frame(modelo          = lap_names,
                   precio_original = original_price,
                   precio_oferta   = strike_price)
  
  # Se agrega la información a la tabla completa
  laptops <- rbind(laptops, aux)
  
  # Se elimina la tabla axiliar
  rm(aux)
}
```



```r
head(laptops)
```

```
##                                                                                         modelo
## 1           Laptop 2 en 1 Lenovo Yoga 530 14 Pulgadas Intel Core i7 8 GB RAM 256 GB Disco Duro
## 2                            Laptop HP 15-db0004 15.6 Pulgadas AMD A9 4 GB RAM 1 TB Disco Duro
## 3              Laptop Lenovo Ideapad 530s 14 Pulgadas Intel Core i7 8 GB RAM 256 GB Disco Duro
## 4                    Laptop Asus Vivobook X505BA 15.6 Pulgadas AMD A9 4 GB RAM 1 TB Disco Duro
## 5  Laptop Dell Inspiron 5570 15.6 Pulgadas Intel Core i3 4 GB+16 GB Optane RAM 1 TB Disco Duro
## 6 Laptop Lenovo Ideapad 330S 14 Pulgadas Intel Core i3 4 GB + 16 GB Optane RAM 1 TB Disco Duro
##   precio_original precio_oferta
## 1           25999       23399.1
## 2           11109        9022.5
## 3           24999       22499.1
## 4           10499        9449.1
## 5           12999       11699.1
## 6           13999       12599.1
```

-----------------------------------------

## 5.4 Limpieza de datos

Notamos dos cosas:

  1. Algunos artículos no tienen precio oferta, por lo cuál el precio oferta se pondrá igual al precio original
  2. Algunos artículos no son _laptops_, son fundas, mochilas o bolsas, los cuáles se deberán de quitar.


```r
laptops %>%
  filter(is.na(precio_oferta)) %>%
  mutate(modelo = substr(modelo,1,20)) %>%
  head(2)
```

```
##                 modelo precio_original precio_oferta
## 1 MacBook Pro Apple 13           29999            NA
## 2 Macbook Pro Touch Ba           41999            NA
```


```r
laptops %>%
  filter(grepl('bolsa', tolower(modelo))) %>%
  mutate(modelo = substr(modelo,1,22)) %>%
  head(2)
```

```
##                   modelo precio_original precio_oferta
## 1 Bolsa para Laptop Cool             599            NA
## 2 Bolsa para Laptop 15.6             599            NA
```



------------------------------

### 5.4.1 Precio oferta = Precio Original en artículos sin oferta

Se pondrá el precio original en el precio oferta para aquellos artículos sin precio oferta


```r
laptops %>%
  mutate(precio_oferta = ifelse(is.na(precio_oferta), precio_original, precio_oferta)) -> laptops
```


------------------------------

### 5.4.2 Dejar sólo laptops

Se quitarán artículos que sean mochilas, bolsas, etc


```r
laptops %>%
  filter(!(grepl('bolsa', tolower(modelo)))) %>%
  filter(!(grepl('funda', tolower(modelo)))) %>%
  filter(!(grepl('mochila', tolower(modelo)))) %>%
  filter(!(grepl('protector', tolower(modelo)))) %>%
  filter(!(grepl('enfriador', tolower(modelo)))) %>%
  filter(!(grepl('maletín', tolower(modelo)))) %>%
  filter(!(grepl('cable', tolower(modelo)))) -> laptops
```


--------------------------------------------

# 6 Análisis de datos

------------------------------------------

## 6.1 Distribución de precio original y precio con oferta - Densidad


```r
laptops %>%
  ggplot() +
  geom_density(aes(precio_original), fill = 'darkblue', colour = 'darkblue', alpha = 0.2) +
  geom_density(aes(precio_oferta), fill = 'darkgreen', colour = 'darkgreen', alpha = 0.2) +
  theme_bw() +
  ggtitle('Distribución Precios - Original vs Oferta') +
  scale_y_continuous(labels = scales::percent) +
  scale_x_continuous(labels = scales::comma) +
  annotate(geom   = 'text',
           x      = 80000,
           y      = 0.00003,
           label  = 'Precio Oferta',
           colour = 'darkgreen') +
  annotate(geom   = 'text',
           x      = 80000,
           y      = 0.000033,
           label  = 'Precio Original',
           colour = 'darkblue')
```

![](scrapping_files/figure-html/unnamed-chunk-22-1.png)<!-- -->

------------------------------------------

## 6.2 Dispersión Precios - original vs oferta


```r
laptops %>%
  ggplot() +
  geom_point(aes(x = precio_original, y = precio_oferta), colour = 'darkblue') +
  theme_bw() +
  ggtitle('Dispersión Precios - Original vs Oferta') +
  scale_y_continuous(labels = scales::comma) +
  scale_x_continuous(labels = scales::comma) +
  geom_abline(slope = 1, intercept = 0)
```

![](scrapping_files/figure-html/unnamed-chunk-23-1.png)<!-- -->

------------------------------------------

## 6.3 Boxplot Cociente Precios - Oferta entre original


```r
laptops %>%
  mutate(ratio = precio_oferta/precio_original) %>%
  ggplot() +
  geom_boxplot(aes(x = '', y = ratio)) +
  theme_bw() +
  ggtitle('Boxplot Ratio Precios - Oferta entre Original') +
  scale_y_continuous(labels = scales::percent)
```

![](scrapping_files/figure-html/unnamed-chunk-24-1.png)<!-- -->


